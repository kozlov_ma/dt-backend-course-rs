FROM rust:latest

WORKDIR /usr/src/app
COPY . .

RUN cargo install --path ./bot && cargo install --path ./web
