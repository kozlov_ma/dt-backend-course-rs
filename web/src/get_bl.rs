use business_logic::BusinessLogic;
use async_once::AsyncOnce;

lazy_static! {
    static ref BUSINESS_LOGIC: AsyncOnce<BusinessLogic> = AsyncOnce::new(async {
        let business_logic = BusinessLogic::from_dotenv()
        .await
        .expect("Couldn't initialize Business Logic from dotenv.");

        business_logic
        });
    }

pub async fn get_business_logic() -> &'static BusinessLogic {
    BUSINESS_LOGIC.get().await
}