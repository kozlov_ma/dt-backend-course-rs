mod get_bl;

use std::fmt::Error;
use actix_web::{get, web, App, HttpServer, ResponseError};
use crate::get_bl::get_business_logic;

#[macro_use]
extern crate lazy_static;

#[get("/me/{user_id}/")]
async fn get_me(path: web::Path<(u64)>) -> actix_web::Result<String> {
    let (user_id) = path.into_inner();
     match get_business_logic().await.get_me(user_id).await {
        Ok(me) => {
            match me {
                None => {
                    Ok("No such user in the database.".to_owned())
                }
                Some(me_str) => {Ok(me_str)}
            }
        }
        Err(_) => {
            Ok("Internal database error.".to_owned())
        }
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| App::new().service(get_me))
        .bind(("127.0.0.1", 8080))?
        .run()
        .await
}
