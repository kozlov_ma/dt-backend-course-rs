use teloxide::{prelude::*, RequestError};
use crate::get_bl::*;
use crate::{bot_get_author, bot_get_username, bot_send_err, business_logic, common_text};

pub async fn handle_start(bot: Bot, msg: Message) -> Result<Message, RequestError> {
    let user = bot_get_author!(bot, msg, common_text::ans::AUTHOR_IS_NONE);
    let user_name = bot_get_username!(bot, msg, user, common_text::ans::USER_NAME_IS_NONE);

    //TODO check if user is already registered
    match business_logic!().register_user(user.id.0, user_name, None::<String>).await {
        Ok(_) => bot.send_message(msg.chat.id, common_text::ans::REGISTRATION_SUCCESSFUL).await,
        Err(_) => bot_send_err!(bot, msg, common_text::ans::DB_ERR)
    }
}
