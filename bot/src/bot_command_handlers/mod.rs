
pub mod me;
pub mod start;
pub mod set_phone;


#[macro_export] macro_rules! bot_send_err {
    ($bot_obj: expr, $msg_obj: expr, $err_text: expr) => {
        $bot_obj.send_message($msg_obj.chat.id, $err_text).await
    };
}

#[macro_export] macro_rules! bot_send_data_option {
    ($bot_obj: expr, $msg_obj: expr, $data: ident, Option<$data_type: ty>, $err_text_if_none: expr) => {
        match $data {
            Some($data) => {
                $bot_obj.send_message($msg_obj.chat.id, $data).await
            },
            None => {
                bot_send_err!($bot_obj, $msg_obj, $err_text_if_none)
            }
        }
    }
}

#[macro_export] macro_rules! bot_get_author {
    ($bot: expr, $msg_obj: expr, $author_none_text: expr) => {
        match $msg_obj.from() {
            None => return bot_send_err!($bot, $msg_obj, $author_none_text),
            Some(user) => user
        }
    };
}

#[macro_export] macro_rules! bot_get_username {
    ($bot: expr, $msg_obj: expr, $user: expr, $username_none_text: expr) => {
        match &$user.username {
            None => return bot_send_err!($bot, $msg_obj, $username_none_text),
            Some(name) => name
        }
    };
}