use teloxide::{prelude::*, RequestError};
use crate::get_bl::*;
use crate::{common_text, bot_send_err, bot_get_author, business_logic, bot_send_data_option};


pub async fn handle_me(bot: Bot, msg: Message) -> Result<Message, RequestError> {
    let user = bot_get_author!(bot, msg, common_text::ans::AUTHOR_IS_NONE);
    let user_id = user.id.0;
    let user_me = business_logic!().get_me(user_id).await;

    match user_me {
        Ok(me) => bot_send_data_option!(bot, msg, me, Option<String>, common_text::ans::NOT_FOUND_IN_DB),
        Err(_) => bot_send_err!(bot, msg, common_text::ans::DB_ERR)
    }
}
