use regex::Regex;
use teloxide::{prelude::*, RequestError};

use crate::get_bl::*;
use crate::{bot_get_author, bot_send_err, business_logic, common_text};

pub async fn handle_set_phone(bot: Bot, msg: Message, phone_number: &str) -> Result<Message, RequestError> {
    if !is_valid_phone_number(phone_number) {
        return bot_send_err!(bot, msg, common_text::ans::INVALID_PHONE_NUMBER);
    }

    let author = bot_get_author!(bot, msg, common_text::ans::AUTHOR_IS_NONE);

    match business_logic!().set_user_phone_number(author.id.0, phone_number).await {
        Ok(_) => bot.send_message(msg.chat.id, common_text::ans::PHONE_NUMBER_SUCCESSFULLY_ADDED).await,
        Err(_) => bot_send_err!(bot, msg, common_text::ans::DB_ERR)
    }
}

fn is_valid_phone_number(phone_number: &str) -> bool {
    let re = Regex::new(r"^\+?\d{1,3}[-\s]?\d{3,}[-\s]?\d{2,}$")
        .expect("This can't be incorrect.");
    re.is_match(phone_number)
}

