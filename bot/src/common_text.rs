pub mod cmd {
    pub const AVAIL_COMMANDS: &str = "Доступные команды:";
    pub const HELP_DESC: &str = "показать этот текст.";
    pub const SETPHONE_DESC: &str = "добавить номер телефона.";
    pub const START_DESC: &str = "начать использовать бота (зарегистрироваться).";
    pub const ME_DESC: &str = "показать информацию о Вас.";
    pub const REG_UNAVAIL: &str = "К сожалению, в данный момент регистрация недоступна.";
}

pub mod ans {
    pub const ID_IS_NONE: &str = "Чел, твой id None..";
    pub const NOT_FOUND_IN_DB: &str = "Вы не найдены в базе данных. Пожалуйста, зарегистрируйтесть с помощью /start";
    pub const DB_ERR: &str = "Произошла ошибка сервера. Пожалуйста, повторите попытку позднее.";
    pub const USER_NAME_IS_NONE: &str = "Чел, твой username None..";
    pub const INVALID_PHONE_NUMBER: &str = "Введён некорректный номер телефона";
    pub const AUTHOR_IS_NONE: &str = "Чел, ты None..";
    pub const PHONE_NUMBER_SUCCESSFULLY_ADDED: &str = "Номер телефона добавлен.";
    pub const REGISTRATION_SUCCESSFUL: &str = "Вы успешно зарегистрированы.";
}

pub mod log {
    pub const BOT_STARTED: &str = "Bot started, apparently..";
    pub const STARTING_BOT: &str = "Starting command bot...";
}

pub mod expect {
    pub const BUSINESS_LOGIC_INIT_FAILED_DENV: &str = "Failed to initialize Business Logic from dotenv";
    pub const TELOXIDE_TOKEN_NOT_FOUND: &str = "TELOXIDE_TOKEN .env var was not set.";
}