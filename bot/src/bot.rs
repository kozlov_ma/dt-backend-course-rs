use teloxide::{prelude::*, utils::command::BotCommands};

use crate::bot_command_handlers;
use crate::common_text;

pub async fn bot_main() {
    pretty_env_logger::init();
    let teloxide_token = dotenvy::var("TELOXIDE_TOKEN")
        .expect(common_text::expect::TELOXIDE_TOKEN_NOT_FOUND);

    log::info!("{}", common_text::log::STARTING_BOT);

    let bot = Bot::new(teloxide_token);
    println!("{}", common_text::log::BOT_STARTED);

    Command::repl(bot, answer).await;
}

async fn answer(bot: Bot, msg: Message, cmd: Command) -> ResponseResult<()> {
    match cmd {
        Command::Help => {
            bot.send_message(msg.chat.id, Command::descriptions().to_string()).await?
        },
        Command::Me => {
            bot_command_handlers::me::handle_me(bot, msg).await?
        },
        Command::SetPhone(phone) => {
            bot_command_handlers::set_phone::handle_set_phone(bot, msg, &phone).await?
        }
        Command::Start => {
            bot_command_handlers::start::handle_start(bot, msg).await?
        }
    };

    Ok(())
}

#[derive(BotCommands, Clone)]
#[command(rename_rule = "lowercase", description = "Доступные команды:")]
enum Command {
    #[command(description = "показать этот текст.")]
    Help,
    #[command(description = "добавить номер телефона.")]
    SetPhone(String),
    #[command(description = "начать использовать бота (зарегистрироваться).")]
    Start,
    #[command(description = "показать информацию о Вас.")]
    Me,
}
