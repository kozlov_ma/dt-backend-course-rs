
use sea_orm::prelude::*;
use sea_orm::*;

use crate::db::{create_user, get_db_conn_from_dotenv, get_user_model_by_id, update_user};
use crate::db_entities::user;


mod db;
mod db_entities;

#[derive(Debug, Clone)]
pub struct BusinessLogic {
    db_conn: DbConn
}

impl BusinessLogic {
    pub async fn from_db_conn(db_conn: DbConn) -> Self {
        Self { db_conn }
    }

    pub async fn from_dotenv() -> Result<Self, DbErr> {
        let db_conn = get_db_conn_from_dotenv().await?;
        Ok(Self { db_conn })
    }

    pub async fn register_user(&self, id: u64, user_name: &str,  phone_number: Option<std::string::String>) -> Result<(), DbErr> {
        let new_user = user::ActiveModel {
            id: ActiveValue::Set(id as i64),
            user_name: ActiveValue::Set(user_name.to_owned()),
            phone_number: ActiveValue::Set(phone_number.to_owned())
        };

        match create_user(&self.db_conn, new_user).await {
            Ok(_) =>  Ok(()),
            Err(err) => Err(err),
        }
    }

    pub async fn set_user_phone_number(&self, user_id: u64, phone_number: &str) -> Result<(), DbErr> {
        let updated_user = user::ActiveModel {
            id: ActiveValue::Set(user_id as i64),
            user_name: ActiveValue::NotSet,
            phone_number: ActiveValue::Set(Some(phone_number.to_owned()))
        };

        match update_user(&self.db_conn, updated_user).await {
            Ok(_) => Ok(()),
            Err(err) => Err(err),
        }
    }

    pub async fn get_me(&self, user_id: u64) -> Result<Option<std::string::String>, DbErr> {
        let user = get_user_model_by_id(&self.db_conn, user_id).await?;
        let user = match user {
            None => return Ok(None::<std::string::String>),
            Some(user) => user
        };

        let phone_number = match user.phone_number {
            None => "не указан.".to_owned(),
            Some(number) => number
        };

        let message = format!("Информация о пользователе:\n\
                Имя пользователя: {}\n\
                Номер телефона: {}",
                              user.user_name,
                              phone_number);

        Ok(Some(message))
    }
}