use sea_orm::prelude::*;
use sea_orm::*;
use crate::db_entities::prelude::User;
use crate::db_entities::user::{ActiveModel, Model};


async fn get_db_conn(connection_url: &str, db_name: &str) -> Result<DbConn, DbErr> {
    let db = Database::connect(connection_url).await?;

    let db = match db.get_database_backend() {
        DbBackend::MySql => {
            db.execute(Statement::from_string(
                db.get_database_backend(),
                format!("CREATE DATABASE IF NOT EXISTS `{}`;", db_name),
            )).await?;

            let url = format!("{}/{}", connection_url, db_name);
            Database::connect(&url).await?
        }
        DbBackend::Postgres => {
            if (db.execute(Statement::from_string(
                db.get_database_backend(),
                format!("CREATE DATABASE \"{}\";", db_name),
            )).await).is_ok() {}; /* CREATE DATABASE db causes error if it already exists,
                 and otherwise doesnt. So we just ignore it. */

            let url = format!("{}/{}", connection_url, db_name);
            Database::connect(&url).await?
        }
        DbBackend::Sqlite => db,
    };

    Ok(db)
}

fn get_dotenv(env_var: &str) -> String {
    dotenvy::var(env_var)
        .unwrap_or_else(|_| panic!("{env_var} env var was not available."))
}

/// Returns a new database built with env vars from .env file.
/// Uses DATABASE_NAME and DATABASE_SERVER_URL vars.
/// PANICS when cannot get env vars by key.
pub async fn get_db_conn_from_dotenv() -> Result<DbConn, DbErr> {
    let database_server = get_dotenv("DATABASE_SERVER");
    let database_user = get_dotenv("DATABASE_USER");
    let database_password = get_dotenv("DATABASE_PASSWORD");
    let database_host = get_dotenv("DATABASE_HOST");
    let database_port = get_dotenv("DATABASE_PORT");
    let database_name = get_dotenv("DATABASE_NAME");

    let connection_url
        = format!("{database_server}://{database_user}:{database_password}@{database_host}:{database_port}");

    get_db_conn(&connection_url, &database_name).await
}


pub async fn get_user_model_by_id(db_conn: &DbConn, id: u64) -> Result<Option<Model>, DbErr> {
    User::find_by_id(id as i64).one(db_conn).await
}

pub async fn update_user(db_conn: &DbConn, user_model: ActiveModel) -> Result<Model, DbErr> {
    User::update(user_model)
        .exec(db_conn)
        .await
}

pub async fn create_user(db_conn: &DbConn, new_user_model: ActiveModel) -> Result<InsertResult<ActiveModel>, DbErr> {
    User::insert(new_user_model)
        .exec(db_conn)
        .await
}

