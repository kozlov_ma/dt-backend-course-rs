pub use sea_orm_migration::prelude::*;

mod m20220101_000001_create_users_table;
mod m20220101_000002_set_id_to_u64;
mod m20230407_210723_switch_to_telegram_username;
mod m20230407_213505_phone_number_nullable;

pub struct Migrator;

#[async_trait::async_trait]
impl MigratorTrait for Migrator {
    fn migrations() -> Vec<Box<dyn MigrationTrait>> {
        vec![
            Box::new(m20230407_213505_phone_number_nullable::Migration),
            Box::new(m20230407_210723_switch_to_telegram_username::Migration),
            Box::new(m20220101_000002_set_id_to_u64::Migration),
            Box::new(m20220101_000001_create_users_table::Migration),
        ]
    }
}
